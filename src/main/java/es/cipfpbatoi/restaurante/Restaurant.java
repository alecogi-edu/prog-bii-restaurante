package es.cipfpbatoi.restaurante;
import es.cipfpbatoi.products.Catalogue;
import es.cipfpbatoi.utils.AnsiColor;
import es.cipfpbatoi.utils.GestorIO;
import es.cipfpbatoi.views.OrderView;
import es.cipfpbatoi.views.OrderViewList;

import java.util.*;

public class Restaurant {

    private ArrayList<Order> orderList;

    private Waiter waiter;

    public Restaurant(Catalogue catalogue) {
        this.orderList = new ArrayList<>();
        this.waiter = new Waiter(catalogue);
    }

    /**
     *  Registra un nuevo pedido en el restaurante
     */
    public void attendClient() {
        Order order =  waiter.attend(getNextOrderCode());
        orderList.add(order);
        System.out.println(AnsiColor.colorize(AnsiColor.GREEN, "Pedido registrado con éxito "));
        showOrder(order);
    }

    /**
     * Lista todas los pedidos
     */
    public void listAllOrders() {
        if (orderList.size() == 0) {
            System.out.println(AnsiColor.colorize(AnsiColor.RED, "No existen pedidos en el restaurante"));
        } else {
            OrderViewList orderViewList = new OrderViewList(new ArrayList<>(orderList));
            System.out.println(orderViewList);
        }
    }

    /**
     * Permite acceder a la información de un pedido
     */
    public void viewOrder() {
        listAllOrders();
        String pregunta = AnsiColor.colorize(AnsiColor.HIGH_INTENSITY, "Introduzca el código de la orden que deseas visualizar: ");
        String orderCode = GestorIO.obtenerCadena(pregunta);
        Order orderBuscada = new Order(orderCode);
        if (!orderList.contains(orderBuscada)) {
            System.out.println(AnsiColor.colorize(AnsiColor.RED, "El pedido introducido no existe"));
        } else {
            showOrder(orderList.get(orderList.indexOf(orderBuscada)));
        }
    }

    /**
     *  Permite marcar una orden como servida
     */
    public void serveOrder() {
        if(!showPendingOrderList()){
            return;
        }
        String pregunta = AnsiColor.colorize(AnsiColor.HIGH_INTENSITY, "Introduzca el código de la orden que desea servir: ");
        String orderCode = GestorIO.obtenerCadena(pregunta);
        Order orderBuscada = new Order(orderCode);
        if (!orderList.contains(orderBuscada)) {
            System.out.println(AnsiColor.colorize(AnsiColor.RED, "El pedido introducido no existe"));
        } else {
            orderList.get(orderList.indexOf(orderBuscada)).setServed();
            System.out.println(AnsiColor.colorize(AnsiColor.GREEN, "El pedido ha sido marcado como servido"));
        }

    }

    /**
     * muestra la lista de ordenes pendientes de servir
     */
    private boolean showPendingOrderList() {
        ArrayList<Order> pendingServedOrderList = new ArrayList<>();
        for (Order orderEntry: orderList) {
            if (!orderEntry.isServed()) {
                pendingServedOrderList.add(orderEntry);
            }
        }
        if (pendingServedOrderList.size() == 0) {
            System.out.println(AnsiColor.colorize(AnsiColor.RED, "No existen pedidos pendientes de servir"));
        } else {
            OrderViewList orderViewList = new OrderViewList(pendingServedOrderList);
            System.out.println(orderViewList);
        }
        return pendingServedOrderList.size() > 0 ;
    }

    private String getNextOrderCode(){
        return "o" + (orderList.size() + 1);
    }

    /**
     * Visualizar una orden en formato tabla
     */
    private void showOrder(Order order) {
        OrderView orderView = new OrderView(order);
        System.out.println(orderView);
    }
}
