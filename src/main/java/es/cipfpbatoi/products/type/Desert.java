package es.cipfpbatoi.products.type;

import java.util.HashSet;
import java.util.List;

public class Desert extends Product {

    public enum Characteristic {
        DIABETIC_SUITABLE, CELIAC_SUITABLE
    }

    private HashSet<Characteristic> characteristic;

    public Desert(String cod) {
        super(cod);
    }

    public Desert(String cod, String name, Characteristic... characteristic) {
        super(cod, name);
        this.characteristic = new HashSet<>(List.of(characteristic));
    }

    public HashSet<Characteristic> getCharacteristic() {
        return characteristic;
    }
}
