package es.cipfpbatoi;

import es.cipfpbatoi.products.Catalogue;
import es.cipfpbatoi.restaurante.MenuApp;
import es.cipfpbatoi.restaurante.Restaurant;

public class App {

    public static void main(String[] args)  {

        Catalogue catalogue = new Catalogue();

        //Creamos el restaurante con la carta
        Restaurant restaurant = new Restaurant(catalogue);

        //Creamos un menu para que gestiones nuestro restaurante
        MenuApp restaurantMenu = new MenuApp(restaurant);
        restaurantMenu.show();

    }

}
